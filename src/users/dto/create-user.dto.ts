import { IsAlpha, IsDate, IsEmail, IsNotEmpty, Max, min, Min} from "class-validator";

export class CreateUserDto {
    
    
    role: string;

    @IsAlpha()
    @IsNotEmpty()
    firstName: string;
    
    @IsAlpha()
    @IsNotEmpty()
    lastName: string;

    // @IsDate()
    @IsNotEmpty()
    birthdate: Date;

    @IsEmail()
    @IsNotEmpty()
    email: string;

    @IsNotEmpty()
    // @Min(15)
    // @Max(30)
    password: string;


    currentHashedRefreshToken?: string;

    access_token?: string;

    // will/can be used as a validation layer
}


