import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, ObjectId } from 'mongoose';
import { Observable } from 'rxjs';
import { User, UserDocument } from 'src/schemas/users.schemas';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import * as bcrypt from 'bcrypt';





@Injectable()
export class UsersService {

  constructor(@InjectModel(User.name) private userModel: Model<UserDocument>){
  }

  async create(createUserDto: CreateUserDto): Promise<User>  {
    return new this.userModel(createUserDto).save();
  }

  async findAll() {
    return this.userModel.find();
  }

  async findOne(id: ObjectId): Promise<User> {
    const user = this.userModel.findById(id);
    if(user) {
      return user
    }
    throw new HttpException('Bruger med dette id findes ikke #1', HttpStatus.NOT_FOUND)
  }

  async findOneByEmail(email: string): Promise<User | undefined>{
    return this.userModel.findOne({email});
  }

  async update(id: ObjectId, updateUserDto: UpdateUserDto): Promise<User> {
    return this.userModel.findByIdAndUpdate(id, updateUserDto);
  }
  
  async remove(id: ObjectId): Promise<User> {
    return this.userModel.findByIdAndDelete(id);
  }

  async removeAll(): Promise<User>{
    
    return this.userModel.remove()
  }

  // used with refresh jwt (not in use)
  async setCurrentRefreshToken(refreshToken: string, userId: ObjectId){
    const currentHashedRefreshToken = await bcrypt.hash(refreshToken, 10);
    await this.update(userId, {
      currentHashedRefreshToken
    });
  }

 


  async getUserIfRefreshTokenMatches(refreshToken: string, userId: ObjectId){
    const user = await this.findOne(userId);

    const isRefreshTokenMathcing = await bcrypt.compare(
      refreshToken,
      user.currentHashedRefreshToken
    );
    if(isRefreshTokenMathcing){
      return user;
    }

  }

  async removeRefreshToken(userId: ObjectId) {
    return this.update(userId, {
      currentHashedRefreshToken: null
    });
  }


}
