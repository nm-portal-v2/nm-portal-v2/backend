import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { User, UserSchema } from 'src/schemas/users.schemas';
import { usersMiddleware } from './users.middleware';
import { AuthService } from 'src/auth/auth.service';


@Module({
  imports: [MongooseModule.forFeature([{ name: "User", schema: UserSchema }])],
  controllers: [UsersController],
  providers: [UsersService],
  exports: [UsersService] // Here we can export other features from this module, to destribute it 
})

//In the user modul export, theres is a a chaining to a middleware module in the same class.
//this is to keep the class responsibility restricted to only handle class routes middleware
// its possible to set this to a global system scale by applying the same methods to the "App.module". go t app.module  
export class UsersModule{}


// MongooseModule.forFeature([{ name: User.name, schema: UserSchema }])