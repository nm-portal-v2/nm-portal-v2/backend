import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';


// example on how we use middleware from structured
const tal = 5;

function name() {
    console.log('This is a middleware call from the users middleware class: ' + tal )
}
// example on how we use middleware from structured
export function myMFunction(req: Request, res: Response, next: NextFunction) {
    console.log('hi from exportet function in middleware')
    next()      
}

// this can be used in a module to have middleware work between the httprequests.
@Injectable()
export class usersMiddleware implements NestMiddleware {
  use(req: Request, res: Response, next: NextFunction) {
    console.log('Request...');
    next()
  }
}

// this class is might not needed


