import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, ObjectId } from 'mongoose';
import { ClinicsDocument, Clinic, ClinicSchema } from 'src/schemas/clinics.schema';
import { callbackify } from 'util';
import { CreateClinicDto } from './dto/create-clinic.dto';
import { UpdateClinicDto } from './dto/update-clinic.dto';


@Injectable()
export class ClinicsService {

  constructor(@InjectModel(Clinic.name) private clinicModel: Model<ClinicsDocument>){}

  async create(createClinicDto: CreateClinicDto) {
    return new this.clinicModel(createClinicDto).save();
  }

  async findAll() {
    return this.clinicModel.find();
  }

  async findOne(id: ObjectId): Promise<Clinic> {
    return this.clinicModel.findById(id);
  }

  async update(id: ObjectId, updateClinicDto: UpdateClinicDto): Promise<Clinic> {
    return this.clinicModel.findByIdAndUpdate(id, updateClinicDto);
  }

  async remove(id: ObjectId): Promise<Clinic> {
    return this.clinicModel.findByIdAndDelete(id);
  }

  async removeAll(): Promise<Clinic> {
    return this.clinicModel.remove();
  }
}
