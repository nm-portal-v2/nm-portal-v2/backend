import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards } from '@nestjs/common';
import { ObjectId } from 'mongoose';
import { JwtAuthGuard } from 'src/auth/Strategies/jwt-auth.guard';
import JwtRefreshGuard from 'src/auth/Strategies/jwt-refresh.guard';
import { ClinicsService } from './clinics.service';
import { CreateClinicDto } from './dto/create-clinic.dto';
import { UpdateClinicDto } from './dto/update-clinic.dto';

@Controller('clinics')
export class ClinicsController {
  constructor(private readonly clinicsService: ClinicsService) {}

  @Post()//works
  async create(@Body() createClinicDto: CreateClinicDto) {
    return this.clinicsService.create(createClinicDto);
  }
  
  @UseGuards(JwtAuthGuard)
  @Get()//works
  findAll() {
    return this.clinicsService.findAll();
  }

  @Get(':id')//work
  findOne(@Param('id') id: ObjectId) {
    return this.clinicsService.findOne(id);
  }

  @Patch(':id')//work
  update(@Param('id') id: ObjectId, @Body() updateClinicDto: UpdateClinicDto) {
    return this.clinicsService.update(id, updateClinicDto);
  }

  @Delete(':id')
  remove(@Param('id') id: ObjectId) {
    return this.clinicsService.remove(id);
  }

  @Delete()
    deleteAllClinics(){
        return this.clinicsService.removeAll()
    }
  

}
