import { Module } from '@nestjs/common';
import { ClinicsService } from './clinics.service';
import { ClinicsController } from './clinics.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Clinic } from './entities/clinic.entity';
import { ClinicSchema } from 'src/schemas/clinics.schema';

@Module({
  imports: [MongooseModule.forFeature([{ name: Clinic.name, schema: ClinicSchema }])],
  controllers: [ClinicsController],
  providers: [ClinicsService],
  exports: [ClinicsService]
})
export class ClinicsModule {}
