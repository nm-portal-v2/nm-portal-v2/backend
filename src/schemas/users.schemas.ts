import { Document, Mongoose, ObjectId, SchemaTypes } from 'mongoose';
import { Prop, raw, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Exclude, Transform } from 'class-transformer';
import { IsMongoId } from 'class-validator';
import { string } from 'joi';


export type UserDocument = User & Document;

// @Schema()
// export class Token extends Document {
//   @Prop({ required: true})
//   value: string;
// }

// export const TokenSchema = SchemaFactory.createForClass(Token)

@Schema()
export class User  {
  
  //important for else ObjectId cant be a string
  @Transform(({ value }) => value.toString())
  _id: ObjectId

  @Prop()
  role: string; 

  @Prop({required: true})
  firstName: string;
  
  @Prop({required: true})
  lastName: string;

  @Prop({required: true})
  birthdate: Date;

  @Prop({required: true, unique: true})
  email: string;
  
  @Prop({minlength: 6})
  password: string; 
  
  @Prop()
  access_token?: string;

  
  @Prop()// used with refresh jwt
  currentHashedRefreshToken?: string;
}



export const UserSchema = SchemaFactory.createForClass(User);

