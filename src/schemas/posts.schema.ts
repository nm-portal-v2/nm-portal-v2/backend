import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

// import { Clinic } from 'src/clinics/entities/clinic.entity';

export type PostsDocument = Post & Document;

@Schema()
export class Post {

  @Prop()
  author: string;

  @Prop() 
  title: string;

  @Prop()
  subject: string;

  @Prop()
  content: string;

  @Prop()
  date: Date;
}

export const PostSchema = SchemaFactory.createForClass(Post);