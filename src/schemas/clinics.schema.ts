import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
// import { Clinic } from 'src/clinics/entities/clinic.entity';

export type ClinicsDocument = Clinic & Document;

@Schema()
export class Clinic {
  @Prop()
  clinicName: string;

  @Prop()
  acuteNumber: Number;

  @Prop()
  phoneNumber: Number;

  @Prop()
  fax: Number
}

export const ClinicSchema = SchemaFactory.createForClass(Clinic);