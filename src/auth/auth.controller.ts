import { Controller, Post, UseGuards, Request, Req, Body, HttpCode, Get, Res, UseInterceptors, ClassSerializerInterceptor, SerializeOptions } from "@nestjs/common";
import { request, response, Response } from "express";
import { CreateUserDto } from "src/users/dto/create-user.dto";
import { AuthService } from "./auth.service";
import { LocalAuthGuard } from "./Strategies/local-auth.guard";
import RequestWithUser from "./requestWithUser.interface";
import { JwtAuthGuard } from "./Strategies/jwt-auth.guard";
import { User, UserSchema } from "src/schemas/users.schemas";
import { Mongoose } from "mongoose";
import { UsersService } from "src/users/users.service";
import { UsersModule } from "src/users/users.module";
import JwtRefreshGuard from "./Strategies/jwt-refresh.guard";



// This controller uses Guard which work as a middleware for our routes. Guard are called on HTTP request as decorators to ensure that the user is authenticated and has a JWT 
// a Controller in nest keeps routes/paths in check by having an 'oversear' path. 
// by having 'auth' in the controller annotation, we makes sure that these routes are only available if they start with auth/
@Controller('auth')
// @UseInterceptors(ClassSerializerInterceptor) // to be removed if refresh doesnt work
export class AuthController{
    constructor(private readonly authService: AuthService, private readonly usersService: UsersService){}

    //  so far going with theis cookie httponly method and reeffresh token
    @HttpCode(200)
    @UseGuards(LocalAuthGuard)
    @Post('login')// Login an user
    async logInTwo(@Req() request: RequestWithUser) {
        const {user} = request;
        const access_token = await this.authService.getTokenWithJwt(user._id);
        const {
            cookie: refreshTokenCookie,
            token: refreshToken
        } = this.authService.getCookieWithRefreshToken(user._id)
        await this.usersService.setCurrentRefreshToken(refreshToken, user._id);
        
        this.usersService.update(user._id, {access_token})
        user.password = undefined
        request.res.setHeader('Set-Cookie', refreshTokenCookie);
        return {user}

    }

    @UseGuards(JwtRefreshGuard)
    @Get('refresh')
    async refresh(@Req() request: RequestWithUser) {
      const {user} = request;
      const accessToken = await this.authService.getTokenWithJwt(user._id);
   
      return {accessToken}
    }


    @UseGuards(JwtAuthGuard)
    @Post('logout')
    @HttpCode(200)
    async logOut(@Req() request: RequestWithUser) {
      await this.usersService.removeRefreshToken(request.user._id);
      console.log('logout test From auth controller')
      request.res.setHeader('Set-Cookie', this.authService.getCookieForLogout());
    //   return response.json({'message': 'logged out'})
      
    }

    // is protected by jwt-auth-guard. (an user need to be authenticated through jwt to access the route)
    // probablu should only be used by user controller

    @UseGuards(JwtAuthGuard)  
    @Get('profile')
    async getProfile(@Request() request: RequestWithUser, @Res() response: Response) {
        const user = request.user
        console.log(user)
        return response.send(user)
    }



    // /register works as a create route. Which might make the create route in "users" irrellevant since only users with the right "role"
    // should be able to perfom the create feature.
    // has the responsibility to create an user based on properties and hash the password
    // Needs JWT guard
    // is protected by jwt-auth-guard. (an user need to be authenticated through jwt to access the route)
    @UseGuards(JwtAuthGuard)
    @Post('register')
    async register(@Body() createUserDto: CreateUserDto){
        return this.authService.register(createUserDto)
    }
}

    // @HttpCode(200) 
    // @UseGuards(LocalAuthGuard)
    // @Post('loginThree')// Login an user
    // async logInLocal(@Req() request: RequestWithUser ) {
    //     const {user} = request
    //     return this.authService.login(user._id)
        
    // }

