import { Strategy } from 'passport-local';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { AuthService } from '../auth.service';
import { User } from 'src/schemas/users.schemas';


// This class use passwport strategy which is a library that supports the verifictation of username with a password. 
// It works as a extra layer to how we authenticate. Instead of using username we use email. 
@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
  constructor(private authService: AuthService) {
    super({ usernameField: 'email' });
  }

  async validate(email: string, password:string): Promise<User>{
      const user = await this.authService.validateUser(email, password);
      
      if(!user){
          throw new UnauthorizedException('Bruger findes ikk i databasen!')
          
      }
      return user
  }
}