import { Request } from 'express';
import { User } from 'src/schemas/users.schemas';

// own class that uses passport by making user to a request
interface RequestWithUser extends Request {
  user: User;
  
}
 
export default RequestWithUser;