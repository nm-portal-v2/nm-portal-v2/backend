import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { UsersService } from 'src/users/users.service';
import * as bcrypt from 'bcrypt';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import { ObjectId } from 'mongoose';
import { TokenPayload } from './tokenPayload.interface';
import { User } from 'src/schemas/users.schemas';
import { UpdateUserDto } from 'src/users/dto/update-user.dto';



@Injectable()
export class AuthService {
    constructor(
        private readonly userService: UsersService,
        private readonly jwtService: JwtService,
        private readonly configService: ConfigService){}

        // user: User

    // public async login(user: any){
    //     const payload = {userEmail: user.email, sub: user.ObjectId}
    //     const token = this.jwtService.sign(payload)
    //     return token;
    // }
    
    // Used to create a cookie

    
   async getTokenWithJwt(userId: ObjectId){
        const payload = { userId };
        const token = this.jwtService.sign(payload)        
        return token;
    }

    //used with refresh (not in use)
    public getCookieWithRefreshToken(userId: ObjectId){
        const payload: TokenPayload = { userId };
        const token = this.jwtService.sign(payload, {
            secret: this.configService.get('JWT_REFRESH_TOKEN_SECRET'),
            expiresIn: `${this.configService.get('JWT_REFRESH_TOKEN_EXPIRATION_TIME')}`
        });
        const cookie = `Refresh=${token}; HttpOnly; Path=/; Max-Age=${this.configService.get('JWT_REFRESH_TOKEN_EXPIRATION_TIME')}`;
        return {
          cookie,
          token
        }
    }

    public getCookieForLogout(){
        return [
            // 'Authentication=; HttpOnly; Path=/; Max-Age=0',
            `Refresh=; HttpOnly; Path=/; Max-Age=0`
        ];
    }


    // Helps with hashing a password when a user is created
    public async register(createUserDto: CreateUserDto){
        const hashedPassword = await bcrypt.hash(createUserDto.password, 12);
        try{
            const createdUser = await this.userService.create({...createUserDto, password: hashedPassword});
            createdUser.password = undefined;
            return createdUser;
        }catch(error){
            if(error){
                throw new HttpException('Der findes allerede en bruger med den Email!', HttpStatus.BAD_REQUEST);
            }
            throw new HttpException('Noget gik galt!', HttpStatus.INTERNAL_SERVER_ERROR);
        }
        
    }

    // Uses the verify passwoword method to check if a user exist by comparing an user email with password, to the hashed password they hold. 
    // with this we can make sure that an user exist and if they password matches
    public async validateUser(email: string, password: string){
        try{
            const user = await this.userService.findOneByEmail(email);
            if(user){
                await this.verifyPassword(password, user.password);
                return user;

            }else{
                console.log('Bruger findes ikke i databasen!')
            }
        }catch(error){
            throw new HttpException('Forkert Password!', HttpStatus.BAD_REQUEST)
        }
    }

    //Compares a password with a hashed password. bcrypt holds a method that does it for us. 
    private async verifyPassword(password: string, hashedPassword: string){
        const isPasswordMatching = await bcrypt.compare(password, hashedPassword);
        if(!isPasswordMatching){
            throw new HttpException('Noget gik galt med password!', HttpStatus.BAD_REQUEST)
        }
    }




    //Authentication is working. planing to setup an authentications controller to structure the features responsibility


    // used before the code above. 
    // async validateUser(email: string, hashedPassword: string): Promise<any>{
    //     try{
    //         const user = await this.userService.findOneByEmail(email);
    //         const isPasswordMatching = await bcrypt.compare(hashedPassword, user.password);
    //         if(!isPasswordMatching){
    //                 throw new HttpException('Wrong credentials provided', HttpStatus.BAD_REQUEST);
    //         }
    //         user.password = undefined;
    //         return user + hashedPassword;
    //     }catch(error){
    //         throw new HttpException('Wrong credentials provided', HttpStatus.BAD_REQUEST)
    //     }
        
    // }


    public async login(userId: ObjectId ){
        const payload = { userId }
        console.log(payload)

        return { 

            access_token: this.jwtService.sign(payload)
        }

    }

}
