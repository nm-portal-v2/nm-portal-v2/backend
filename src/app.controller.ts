import { Controller, Get, Post, UseGuards, Request } from '@nestjs/common';
import { AppService } from './app.service';


@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  //Controller som håndtere
  @Get()
  getHello(): string {
    return this.appService.getHello();
  }




}
