import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import * as Joi from 'joi';
import { ConfigModule } from '@nestjs/config';
import { ClinicsModule } from './clinics/clinics.module';
import { join } from 'path/posix';
import { PostsModule } from './posts/posts.module';


@Module({
  imports: [ MongooseModule.forRoot('mongodb://localhost/NMPortalDB'),UsersModule, AuthModule, ClinicsModule,ConfigModule.forRoot({
    validationSchema: Joi.object({
      JWT_SECRET: Joi.string().required(),
      JWT_EXPIRATION_TIME: Joi.string().required(),
      JWT_REFRESH_TOKEN_SECRET: Joi.string().required(),
      JWT_REFRESH_TOKEN_EXPIRATION_TIME: Joi.string().required()
    })
  }), PostsModule],
  controllers: [AppController],
  providers: [AppService],
})




// In the export of app.module we have applied the same method from the user.module to demonstrate that responsbility can be set to a global scale, 
// and still remain restricted to the individual route handlers. 
export class AppModule {}


// a way of implementing middleware for routes. Should be 
// implements NestModule {
//   configure(consumer: MiddlewareConsumer){
//     consumer.apply(myMFunction, usersMiddleware).forRoutes(UsersController)
//   }